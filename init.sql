DROP database if exists keanu;
CREATE DATABASE keanu;

USE keanu;

CREATE TABLE film (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(25),
    personage VARCHAR(25),
    imgURL VARCHAR(255),
    jour VARCHAR(255)

);

GRANT ALL PRIVILEGES ON keanu.* TO 'ovsep'@'localhost';

INSERT INTO film (titre, personage, imgURL)
VALUES ('John Wick', 'Neo', 'https://m.media-amazon.com/images/I/71rm1PuuM2L._SL1200_.jpg')

